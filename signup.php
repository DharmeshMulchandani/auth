<?php
require_once('./app/init.php');
if(isset($_POST['signup'])){
    $validator->check($_POST, [
        'email' => [
            'required' => true,
            'email' => true,
            'minlength' => 5,
            'maxlength' => 255,
            'unique' => 'users'
        ],
        'username' => [
            'required' => true,
            'maxlength' => 20,
            'unique' => 'users'
        ],
        'password' => [
            'required' => true,
            'password' => true,
            'minlength' => 3
        ]
    ]);

    if(!$validator->fails()) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        User::create([
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'verify' => 0,
            'twofactor' => 0,
            'otp' => 0
        ]);
        Auth::signin($username, $password);
        redirect("secure-page.php");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="style/main.css">
</head>
<body>
    <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST" class="d-grid">
        <div class="form-image">
            <img src="images/SignUp.png" class="img-responsive">
        </div>
        <div class="form-fields d-flex">
            <h1 class="title">Sign up</h1>
            <div class="form-input">
                <input type="text" name="username" id="username" placeholder="Username">
                <span><?=$validator->errors()->has('username') ? $validator->errors()->first('username') : '';?></span>
            </div>
            <div class="form-input">
                <input type="email" name="email" id="email" placeholder="Email">
                <span class="error-msg"><?=$validator->errors()->has('email') ? $validator->errors()->first('email') : '';?></span>
            </div>
            <div class="form-input">
                <input type="password" name="password" id="password" placeholder="Password">
                <span><?=$validator->errors()->has('password') ? $validator->errors()->first('password') : '';?></span>
            </div>
            <div class="form-input">
                <input type="submit" name="signup" class="primary-btn">
            </div>
        </div>
    </form>
</body>
</html>