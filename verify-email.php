<?php
require_once("./app/init.php");
if(isset($_GET['s']))
{
$userId = Auth::user()->id;
$verifyToken = $token->createVerifyToken($userId);
$email = Auth::user()->email;
$mail = Mail::getMailer('noreply@fulltimepass.com');
$mail->addAddress($email);
$mail->Subject = 'Password Recovery';
$mail->Body = <<<MAIL_BODY
    <p>Use the below link to verify your account!</p>
    <p><a href = 'http://localhost:9999/verify.php?t={$verifyToken->token}'>Click Here</a></p>
MAIL_BODY;

if($mail->send()) {
    echo "Mail has been sent! Please check your inbox!";
} else {
    echo "There is some issue with the server! Please try again in some time";
}
}
else 
{
    die("How the hell you reached here!");
}
?>