<?php
class User {
    static public Database $database;
    static protected string $table="users";

    protected $attb = array();

    //is run when writing data to inaccessible (protected or private) or non-existing properties
    public function __set($name, $value)
    {
        $this->attb[$name] = $value;
    }

    //is utilized for reading data from inaccessible (protected or private) or non-existing properties.
    public function __get($name)
    {
        return $this->attb[$name] ?? null;
    }

    public static function build() 
    {
        $sql = "CREATE TABLE IF NOT EXISTS ". static::$table . " (id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, email VARCHAR(255) NOT NULL UNIQUE, username VARCHAR(20) NOT NULL UNIQUE, password VARCHAR(255) NOT NULL, verify VARCHAR(20), twofactor VARCHAR(20), otp VARCHAR(20))";
        self::$database->query($sql);
    }

    static public function create(mixed $data): mixed
    {
        if(!isset($data['password'])) {
            return false;
        }

        $data['password'] = Hash::make($data['password']);
        return self::$database->table(self::$table)->insert($data);
    }

    public function save(): bool
    {
        return $this->create($this->attb);
    }


    public static function updatePassword(string $newPassword, int $userId)
    {
        return self::$database->table(self::$table)
                        ->where('id', $userId)
                        ->update(['password'=>Hash::make($newPassword)]);
    }

    public static function findByUserName(string $username)
    {
        return self::$database->table(self::$table)
                        ->where('username', $username)
                        ->first();
    }

    static public function findByEmail(string $email)
    {
        return self::$database->table(self::$table)
                        ->where('email', $email)
                        ->first();
    }

    static public function find(string $userId)
    {
        return self::$database->table(self::$table)
                        ->where('id', $userId)
                        ->first();
    }

    public static function updateUserVerify($userId)
    {
        self::$database->table("users")
                        ->where('id', $userId)
                        ->update(['verify' => 1]);
    }

    public static function removeTwoFactorAuthentication($userId)
    {
        return self::$database->table(self::$table)
                        ->where('id', $userId)
                        ->update(['twofactor' => 0]);
    }

    public static function setOtp($otp, $userId)
    {
        return self::$database->table(self::$table)
                        ->where('id', $userId)
                        ->update(['otp' => $otp]);
    }

    public static function updateTwoFactor($userId)
    {
        return self::$database->table(self::$table)
                        ->where('id', $userId)
                        ->update(['twofactor' => 1]);
    }

    public static function deleteOtp($userId)
    {
        return self::$database->table(self::$table)
                        ->where('id', $userId)
                        ->update(['otp' => 0]);
    }

}
?>