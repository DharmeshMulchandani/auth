<?php
class Database {
    protected PDO $pdo;
    protected $dataBag = array();
    protected string $table;
    protected string $where;

    /**
     * PDO::FETCH_OBJ :-
     * Return next row as an anonymous object with column names as properties
     * 
     * setAttribute() :-
     * Sets an attribute on the database handle.
     * 
     * PDO::FETCH_BOTH (default):-
     * Returns an array indexed by both column name and 0-indexed column number as returned in your result set
     * 
     */
    public function __construct($host = 'localhost', $db = 'auth', $port = 3306, $username='dharmesh', $passsword = 'dharmesh', $fetchMode = PDO::FETCH_OBJ) 
    {
        $this->pdo = new PDO("mysql:host={$host};port={$port};dbname={$db}",$username, $passsword);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, $fetchMode);
        //$this->where = "1";
        $this->resetFields();
    }

    /**
     * Used to hit DDL Statement
     * 
     * Prepares and executes an SQL statement
    */

    public function query($sql)
    {
        //dd($this->pdo->query($sql));
        return $this->pdo->query($sql);
    }

    /**
      * Use this to fire Raw Select Queries
    */

    public function rawQueryExecutor($sql)
    {
        return $this->query($sql)->fetchAll();
    }

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function where(string $field, string $value, string $operator="=")
    {
        $this->dataBag["WHERE$field"] = $value;
        if($this->where === "1") {
            $this->where = "$field $operator :WHERE$field";
        } else {
            $this->where = $this->where . " AND $field $operator :WHERE$field";
        }
        return $this;
    }

    public function get($columns = "*")
    {
        $sql = $this->prepareSQLQuery($columns);
        $ps = $this->pdo->prepare($sql); //prepare an SQL statement to be executed
        $ps->execute($this->dataBag); //executes a prepared statement
        return $ps->fetchAll();
    }

    public function first($columns = "*")
    {
        $sql = $this->prepareSQLQuery($columns);
        $sql = $sql . " LIMIT 0, 1";
        // dd($sql);
        $ps = $this->pdo->prepare($sql);
        // dd($this->dataBag);
        $ps->execute($this->dataBag);
        // dd($ps);
        $this->resetFields();
        $data = $ps->fetchAll();
        return !empty($data) ? $data[0] : null;
    }

    public function count()
    {
        $as = "count";
        $sql = "SELECT COUNT(*) as $as FROM {$this->table} WHERE {$this->where}";
        $ps = $this->pdo->prepare($sql);
        $ps->execute($this->dataBag);
        $this->resetFields();
        return $ps->fetchAll()[0]->$as;
    }

    public function insert($data)
    {
        $keys = array_keys($data);

        $fields = "`" . implode("`, `", $keys) . "`";
        $placeholder = ":" . implode(", :", $keys);

        $sql = "INSERT INTO {$this->table} ($fields) VALUES($placeholder)";
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        return $ps->execute($data);
    }

    public function update(array $data)
    {
        $updationString = "";
        foreach($data as $key=>$value) {
            $updationString = $updationString . " $key = :$key,";
        }
        $updationString = rtrim($updationString, ",");
        $sql = "UPDATE {$this->table} SET {$updationString} WHERE {$this->where}";
        $dataWithConditionalParams = array_merge($data, $this->dataBag);
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        //dd()
        return $ps->execute($dataWithConditionalParams);
    }

    public function delete()
    {
        $sql = "DELETE FROM {$this->table} WHERE {$this->where}";
        $dataParams = $this->dataBag;
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        // dd($ps->execute($dataParams));
        return $ps->execute($dataParams);
    }

    public function exists(mixed $data)
    {
        foreach($data as $key => $value)
        {
            $this->where($key, $value);
        }

        return $this->count() >=1 ? true : false;
    }

    private function prepareSQLQuery($columns)
    {
        return "SELECT $columns FROM {$this->table} WHERE {$this->where}";
    }

    private function resetFields()
    {
        $this->table = "";
        $this->where = "1";
        $this->dataBag = array();
    }
}
?>