<?php
define("USER_KEY", "logged_in_user");

class Auth 
{
    static public function signin($username, $password) : bool
    {
        $dbUser = User::findByUsername($username);
        if($dbUser) {
            $hashedPassword = $dbUser->password;
            if(Hash::verify($password, $hashedPassword)) {
                self::setLoggedInUser($dbUser);
                return true;
            }
        }
        return  false;
    }

    static public function setLoggedInUser($dbUser)
    {
        $_SESSION[USER_KEY] = $dbUser;
    }

    static public function signout()
    {
        unset($_SESSION[USER_KEY]);
    }

    static public function user()
    {
        if(isset($_SESSION[USER_KEY])) {
            return $_SESSION[USER_KEY];
        }
        return null;
    }
}
?>