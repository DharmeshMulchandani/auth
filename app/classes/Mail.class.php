<?php
use PHPMailer\PHPMailer\PHPMailer;

require('vendor/autoload.php');

class Mail
{
    public static function getMailer($fromAddress, $fromName="Admin"): PHPMailer
    {
        $mail = new PHPMailer();

        $mail->isSMTP();
        $mail->Host = "sandbox.smtp.mailtrap.io";
        $mail->SMTPAuth = true;
        $mail->Username   = '948d5fed547ca1';                     
        $mail->Password   = '61e4f2e365444a';
        $mail->Port       = 2525;                               
        $mail->SMTPSecure = 'tls';

        $mail->isHTML();
        $mail->setFrom($fromAddress, $fromName);

        return $mail;
    }
}
?>