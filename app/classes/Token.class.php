<?php
class Token
{
    protected $types = [
        'REMEMBER_ME' => 0,
        'FORGOT_PASSWORD' => 1,
        'VERIFY_TOKEN' => 2,
        'TWO_FACTOR' => 3
    ];

    protected Database $database;
    private static $REMEMBER_ME_EXPIRY_TIME = '30 minutes';
    private static $FORGOT_PASSWORD_EXPIRY_TIME = '10 minutes';
    private static $VERIFY_TOKEN_EXPIRY_TIME = '10 minutes';
    private static $TWO_FACTOR_EXPIRY_TIME = '5 minutes';
    public static $REMEMBER_ME_EXPIRY_TIME_IN_SECS = 1800;
    private string $table = "tokens";

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function build()
    {
        $sql = "CREATE TABLE IF NOT EXISTS {$this->table}(id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, user_id INT, token VARCHAR(255) UNIQUE, expires_at DATETIME NOT NULL, type TINYINT)";
        $this->database->query($sql);
    }

    // public function getValidExistingToken(int $userId, int $type)
    // {
    //     if(! in_array($type, array_values($this->types))) {
    //         return null;
    //     }

    //     $token = $this->database->rawQueryExecutor("SELECT * FROM {$this->table} WHERE user_id = $userId AND type = {$type} AND expires_at >= now()");

    //     return $token;
    // }

    // public function getValidExistingToken(int $userId){
    //     return $this->getValidExistingRememeberMeToken($userId,$this->types['REMEMBER_ME']);
    // }
    private function getValidExistingToken(int $userId, int $type){
        if(!in_array($type,array_values($this->types))){
            return null;
        }
        $token = $this->database->rawQueryExecutor("SELECT * FROM {$this->table} WHERE user_id = $userId AND type = $type AND expires_at >= now()");
        return $token;
    }

    public function getValidRememberMeToken(int $userId) {
        return $this->getValidExistingToken($userId, $this->types['REMEMBER_ME']);
    }

    public function getValidForgotPasswordToken(int $userId) {
        return $this->getValidExistingToken($userId, $this->types['FORGOT_PASSWORD']);
    }

    public function getValidVerifyToken(int $userId) {
        return $this->getValidExistingToken($userId, $this->types['VERIFY_TOKEN']);
    }

    public function getValidTwoFactorToken(int $userId) {
        return $this->getValidExistingToken($userId, $this->types['TWO_FACTOR']);
    }

    public function createRememberMeToken(int $userId)
    {
        $current = date('Y-m-d H:i:s');
        $expiryTime = date('Y-m-d H:i:s', strtotime($current . "+". Token::$REMEMBER_ME_EXPIRY_TIME));
        return $this->createToken($userId, $this->types['REMEMBER_ME'], $expiryTime);
    }

    public function createForgotPasswordToken(int $userId)
    {
        $current = date('Y-m-d H:i:s');
        $expiryTime = date('Y-m-d H:i:s', strtotime($current . "+". Token::$FORGOT_PASSWORD_EXPIRY_TIME));
        return $this->createToken($userId, $this->types['FORGOT_PASSWORD'], $expiryTime);
    }

    public function createVerifyToken(int $userId)
    {
        $current = date('Y-m-d H:i:s');
        $expiryTime = date('Y-m-d H:i:s', strtotime($current . "+". Token::$VERIFY_TOKEN_EXPIRY_TIME));
        return $this->createToken($userId, $this->types['VERIFY_TOKEN'], $expiryTime);
    }

    public function createTwoFactorToken(int $userId)
    {
        $current = date('Y-m-d H:i:s');
        $expiryTime = date('Y-m-d H:i:s', strtotime($current . "+". Token::$TWO_FACTOR_EXPIRY_TIME));
        return $this->createToken($userId, $this->types['TWO_FACTOR'], $expiryTime);
    }

    private function createToken(int $userId, int $type, string $expiryTime)
    {
        $token = $this->getValidExistingToken($userId, $type);
        if($token != null && !empty($token)) {
            return $token[0];
        }

        // $current = date('Y-m-d H:i:s');
        // $expiryTime = date('Y-m-d H:i:s', strtotime($current . "+". Token::$REMEMBER_ME_EXPIRY_TIME));
        $token = Hash::generateRandomToken($userId);

        $data = [
            'user_id' => $userId,
            'token' => $token,
            'expires_at' => $expiryTime,
            'type' => $type
        ];

        return $this->database->table($this->table)->insert($data) ? (object)$data : null;
    }

    public function deleteRememberMeToken(int $userId, bool $deleteOnlyValid = false)
    {
        return $this->deleteToken($userId, $this->types['REMEMBER_ME'], $deleteOnlyValid); 
    }

    public function deleteForgotPasswordToken(int $userId, bool $deleteOnlyValid = false)
    {
        return $this->deleteToken($userId, $this->types['FORGOT_PASSWORD'], $deleteOnlyValid); 
    }

    public function deleteVerifyToken(int $userId, bool $deleteOnlyValid = false)
    {
        return $this->deleteToken($userId, $this->types['VERIFY_TOKEN'], $deleteOnlyValid); 
    }

    public function deleteTwoFactorToken(int $userId, bool $deleteOnlyValid = false)
    {
        return $this->deleteToken($userId, $this->types['TWO_FACTOR'], $deleteOnlyValid); 
    }

    private function deleteToken(int $userId, int $type, bool $deleteOnlyValid)
    {
        $queryBuilder = $this->database->table($this->table)->where('user_id', $userId)->where('type', $type);
        // dd($queryBuilder);
        if($deleteOnlyValid) {
            $current = date('Y-m-d H:i:s');
            $queryBuilder = $queryBuilder->where('expires_at', $current, '>=');
        }
        return $queryBuilder->delete();
    }

    public function isValidRememberMe(string $userToken)
    {
        return $this->isValid($userToken, $this->types['REMEMBER_ME']);
    }

    public function isValidForgotPassword(string $userToken)
    {
        return $this->isValid($userToken, $this->types['FORGOT_PASSWORD']);
    }

    public function isValidVerify(string $userToken)
    {
        return $this->isValid($userToken, $this->types['VERIFY_TOKEN']);
    }

    public function isValidTwoFactor(string $userToken)
    {
        return $this->isValid($userToken, $this->types['TWO_FACTOR']);
    }

    private function isvalid(string $userToken, int $type) {
        $current = date('Y-m-d H:i:s');
        return $this->database->table($this->table)
        ->where("token", $userToken)
        ->where("expires_at", $current, ">=")
        ->where("type", $type)
        ->first();
    }
}
?>