<?php
require_once("./app/init.php");
if(isset($_POST['verifyOtp']))
{
    $userOtp = $_POST['textOtp'];
    $verifyTwoFactorToken = $_POST['hide'];
    // dd($verifyTwoFactorToken);
    $user = User::find(Auth::user()->id);
    Auth::setLoggedInUser($user);
    $otp = Auth::user()->otp;
    $userId = Auth::user()->id;
    
    if(checkOtp($otp, $userOtp))
    {
        User::updateTwofactor(Auth::user()->id);
        $user = User::find(Auth::user()->id);
        Auth::setLoggedInUser($user);
        User::deleteOtp($userId);
        // dd($verifyTwoFactorToken);
        $validTwoFactorToken = $token->isValidTwoFactor($verifyTwoFactorToken);
        if($validTwoFactorToken)
        {
            $token->deleteTwoFactorToken($userId);
            redirect('secure-page.php');
        }
        else
        {
            redirect("sign-in.php");
        }
    }

    else 
    {
        echo "Otp didn't matched";
    }
} 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
        <input type="text" name="textOtp">
        <input type="hidden" name="hide" value="<?=$_GET['twof']?>">
        <input type="submit" name="verifyOtp">Verify OTP
    </form>
</body>
</html>