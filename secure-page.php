<?php
require_once("./app/init.php");
secure($token);
$verify = Auth::User()->verify;
$twoFact = Auth::User()->twofactor;

if(isset($_GET['d']))
{
    $user = User::find(Auth::User()->id);
    Auth::setLoggedInUser($user);
}

if(isset($_POST['unchecked']))
{
    $userId = Auth::user()->id;
    User::removeTwoFactorAuthentication($userId);
    $user = User::find($userId);
    Auth::setLoggedInUser($user);
}

$twoFactor = isset($_POST['enableOtp']) ? true  : false;

if($twoFactor)
{
    redirect('send-otp.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Secured Page</title>
</head>
<body>
    <h1>Welcome <?=AUTH::user()->username;?></h1>
    <p>This is secured page, You cannot acces it without login</p>
    <a href="change-password.php">Change Password</a>
    <a href="./sign-out.php">Sign Out</a>
    <?php
    if($verify !=1 )
    {
    ?>
    <a href="verify-email.php?s">Verify User</a>
    <?php
    } else {
        echo "You are verifyed user";
    }
    ?>

    <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST" id="checked-form">
        Enable 2 Factor Authentication <input type="checkbox" name="enableOtp" id="enableOtp" <?= $twoFact == 1 ? 'checked' : ''; ?>>
    </form>

    <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST" id="unchecked-form">
        <input type="hidden" name="unchecked" id="unchecked">
    </form>

    <script>
        document.getElementById('enableOtp').addEventListener('change', function(){
            if(this.checked) {
                document.getElementById('checked-form').submit();
            } else {
                document.getElementById('unchecked-form').submit();
            }
        });
    </script>
</body>
</html>