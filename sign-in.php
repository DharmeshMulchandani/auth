<?php
require_once("./app/init.php");
// $database->table("users")
//          ->insert(['email' => 'dm@gmail.com', 'password' => 'pass']);

// $data = $database->table("users")
//         ->where('email', '%n%', 'like')
//         ->update(['password' => 'dm']);
// dd($data);

if(isset($_POST['signin'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = isset($_POST['remember']) ? true : false;
    
    if(Auth::signin($username, $password)) {
        if($rememberMe) {
            $userToken = $token->createRememberMeToken(Auth::user()->id);
            setcookie("rememberme", $userToken->token, time() + Token::$REMEMBER_ME_EXPIRY_TIME_IN_SECS);
        }

        $twoFactor = Auth::user()->twofactor;

        if($twoFactor == '1')
        {
            // dd("Remember");
            redirect('send-otp.php');
        }
        else {
            redirect("secure-page.php");
        }
    }
    else {
        echo "Username/Password not match....";
    }
}

if(secure($token, false))
{
    redirect("secure-page.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="style/main.css">
</head>
<body>
    <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST" class="d-grid">
        <div class="form-image">
            <img src="images/SignIn.png" class="img-responsive">
        </div>
        <div class="form-fields d-flex">
            <h1 class="title">Sign In</h1>
            <div class="form-input">
                <input type="text" name="username" id="username" placeholder="Username"/>
            </div>
            <div class="form-input">
                <input type="password" name="password" id="password" placeholder="Password"/>
            </div>
            <div class="d-flex">
                <input type="checkbox" name="remember" id="remember"/>
                <label for="remember">Remember Me</label>
            </div>
            <div class="form-input">
                <input type="submit" name="signin" value="Sign in" class="primary-btn"/>
            </div>
        </div>
        
    </form>
</body>
</html>