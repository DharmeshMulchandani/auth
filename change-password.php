<?php
require_once("./app/init.php");
secure($token);
if(isset($_POST['change']))
{
    $validator->check($_POST, [
        'currentpassword' => [
            'required' => true,
            'password' => true,
            'minlength' => 3
        ]
    ]);

    $currentPassword = $_POST['currentpassword'];
    $newPassword = $_POST['newpassword'];
    $cnfPassword = $_POST['cnfpassword'];

    $userId = Auth::user()->id;

    if($newPassword != $cnfPassword)
    {
        echo "New password and Conform Password Does Not match";
    }
    else
    {
        $hashPassword = Hash::make($cnfPassword);

        $password = Auth::user()->password;
        $userName = Auth::user()->username;
        
        $verifyPassword = Hash::verify($currentPassword, $password);

        if($verifyPassword)
        {
            $dataUpdate = $database->table("users")
                    ->where('username', $userName)
                    ->update(['password' => $hashPassword]);
            
            $dataDelete = $database->table("tokens")
                    ->where('user_id', $userId)
                    ->delete();
            setcookie("rememberme", "", time() - 3600);
            redirect('sign-in.php');
            
        }

        else
        {
            echo "Password didn't match";
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Change-Password</title>
    <link rel="stylesheet" href="style/main.css"/>
</head>
<body>
    <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">
        <div class="main-container d-grid">
            <div class="form-image">
                <img src="images/ResetPass.png" alt="forgot-password image" class="img-responsive">
            </div>
            <div class="form-fields d-flex">
                <div class="form-input">
                    <input type="password" name="currentpassword" placeholder="Current Password"/>
                    <span><?=$validator->errors()->has('currentpassword') ? $validator->errors()->first('currentpassword') : '';?></span>
                </div>
                <div class="form-input">
                    <input type="password" name="newpassword" placeholder="New Password"/>
                    <span><?=$validator->errors()->has('password') ? $validator->errors()->first('password') : '';?></span>
                </div>
                <div class="form-input">
                    <input type="password" name="cnfpassword" placeholder="Conform New Password"/>
                    <span><?=$validator->errors()->has('password') ? $validator->errors()->first('password') : '';?></span>
                </div>
                <div class="form-input">
                    <input type="submit" name="change" class="primary-btn"/>
                </div>
                
                <h1><?=AUTH::user()->username;?></h1>
            </div>
        </div>
    </form>
</body>
</html>