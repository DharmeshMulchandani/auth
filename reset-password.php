<?php
require_once 'app/init.php';
if(isset($_GET['t']))
{
    $userToken = $_GET['t'];
    $tokenObject = $token->isValidForgotPassword($userToken);

    if($tokenObject) {
?>
<form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">
        <input type="password" name="password">
        <input type="text" name="token" value="<?=$tokenObject->token;?>">
        <br><br>
        <input type="submit" value="Reset Password" name = "reset_password">
</form>

<?php
    } else {
        die("Your Token Expired");
    }
}
else if(isset($_POST['reset_password']))
{
    $password = $_POST['password'];
    $tokenObject = $token->isValidForgotPassword($_POST['token']);
    if($tokenObject) {
        $userId = $tokenObject->user_id;
        $result = User::updatePassword($password, $userId);
        
        if($result) {
            // dd($userId);
            $token->deleteForgotPasswordToken($userId);
            echo "Password Changed";
            // dd($token->deleteForgotPasswordToken($userId));
            ?>
            <!-- <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="refresh" content="3; URL=http://localhost:9999/secure-page.php">
                <title>Updated</title>
            </head>
            <body>
                Password Updated
                You will redirect here in some seconds
            </body>
            </html> -->
            <?php
        } else {
            die("Please retry after some time!");
        }
    }
    else
    {
        die("Your Token Expired");
    }
}
else
{
    die("How the hell you reached here?");
}
?>