<?php
require_once("./app/init.php");
if(isset($_GET['t']))
{
    $verifyToken = $_GET['t'];
    $tokenObject = $token->isValidVerify($verifyToken);
    $userId = Auth::user()->id;
    $user = User::find($userId);
    User::updateUserVerify($userId);
    if($tokenObject)
    {
        $token->deleteVerifyToken($userId);
        redirect('secure-page.php?d');
    }
    else {
        echo "Your token is expired!";
    }
}
else 
{
    die("How the hell you reached here!");
}
?>