<?php
require_once("./app/init.php");
$otp = generateOtp();
$userId = Auth::user()->id;
User::setOtp($otp, $userId);
$email = Auth::user()->email;
$mail = Mail::getMailer('noreply@fulltimepass.com');
$mail->addAddress($email);
$mail->Subject = 'OTP';
$mail->Body = <<<MAIL_BODY
<p>Your OTP</p>
<p>For Verification: $otp </p>
MAIL_BODY;

if($mail->send()) {
   $twoFactor =  $token -> createTwoFactorToken($userId);
   // dd($twoFactor);
   redirect('otp-check.php?twof=' . $twoFactor->token);
} else {
    echo "There is some issue with the server! Please try again in some time";
}
?>